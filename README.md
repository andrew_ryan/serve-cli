# This is a file serving and file upload service in the local area network (LAN) CLI
[![Crates.io](https://img.shields.io/crates/v/loa.svg)](https://crates.io/crates/serve-cli)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/serve-cli)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/serve-cli/-/raw/master/LICENSE)

## install

```
cargo install serve-cli
```

```bash
$ serve-cli

# for browser serving local files
[INFO  serve_cli] Server listening on http://172.20.10.2:8787

# upload file fontend
[INFO  serve_cli] Server listening on http://172.20.10.2:8585

# also can upload file from curl
curl --location --request POST 'http://172.20.10.2:8585/' --data-binary '@/home/andrew/Documents/wordlist1.txt'

```

![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/b63b390cbe544a5fb5a6f51ced4df1c6~tplv-k3u1fbpfcp-watermark.image?)

![image.png](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/d16f2943c9404e4184218075892218f7~tplv-k3u1fbpfcp-watermark.image?)
