#![feature(async_closure)]
use serve_cli::start_all_server;
/// Program Entry Point
#[tokio::main]
async fn main() {
    start_all_server::start().await;
}
